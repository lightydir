Lightydir

Lightydir is a replacement for LightTPD's default directory
listing. It supports the following features,

 - Show TITLE and HTML prelude from `README.txt` in each directory
 - Use [markdown](http://daringfireball.net/projects/markdown/syntax) 
 - Auto-discover [bzr](http://bazaar-vcs.org/) branches and provide the branch URL.

TODO

 - Integrate with `bzrlib` (show rev, log, etc.. for bzr branch)
 

