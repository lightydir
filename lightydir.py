# lightydir.py

__author__ = 'Sridhar Ratnakumar <srid@nearfar.org>'

import os, stat, time
import markdown
import web 
web.webapi.internalerror = web.debugerror


class File(object):

    def __init__(self, path):
        if os.path.isdir(path) and not path.endswith(os.path.sep): 
            path += os.path.sep
        self.path = path

    def name(self):
        rest, name = os.path.split(self.path)
        if name == '':
            rest, name = os.path.split(rest)
        return name

    def cgi_name(self):
        if os.path.isdir(self.path):
            return self.name() + "/"
        else:
            return self.name()
    
    def parents(self):
        """
        >>> for p in File("/home/srid/foo/bar/").parents():
        print p
        /home/srid/foo/bar/
        /home/srid/foo/
        /home/srid/
        /home/
        /
        >>>
        """
        path = self.path
        yield path
        if path.endswith("/"):
            path = path[:-1]
        while path:
            path, rest = path.rsplit(os.path.sep, 1)
            yield path + "/"

    def is_hidden_file(self):
        name = self.name()
        return name.startswith('.') or name.endswith('~')

    def last_modified(self):
        t = os.stat(self.path)[stat.ST_MTIME]
        return time.strftime("%d-%b-%Y %H:%M", time.localtime(t))

    # from Trac source
    @staticmethod
    def pretty_size(size):
        jump = 512
        if size < jump:
            return "1k"

        units = ['k', 'M', 'GB', 'TB']
        i = 0
        while size >= jump and i < len(units):
            i += 1
            size /= 1024.0

        return "%.1f%s" % (size, units[i - 1])

    def size(self):
        return File.pretty_size(os.stat(self.path)[stat.ST_SIZE])



class lightydir:
    def GET(self):
        web.header("Content-Type","%s; charset=utf-8" % 'text/html')
        self.REQUEST_URI   = path = os.environ.get('REQUEST_URI')
        self.DOCUMENT_ROOT = os.environ.get('DOCUMENT_ROOT')
        self.DIR_PATH = os.path.join(self.DOCUMENT_ROOT, self.REQUEST_URI[1:])
        
        self.title = "Index of %s" % self.REQUEST_URI
        self.body = ""
        self.description = {}

        self.readme()

        body = self.body
        filelist = os.listdir(self.DIR_PATH)
        
        p, bzrbranch = self.bzrbranch()
        if bzrbranch:
            body += '<div id="bzr">'
            body += '   <p>You can get the source code for this branch via: </p>'
            body += '    <code>'
            body += '    <a href="http://bazaar-vcs.org/">bzr</a> get %s' %  bzrbranch
            body += '    </code>'
            body += '</div>'
            
        trs = ['<tr><td><a href="..">..</a></td></tr>']
        filelist.sort()
        for file_name in filelist:
            file = File(os.path.join(self.DIR_PATH, file_name))
            if not file.is_hidden_file():
                file_lm = file.last_modified()
                file_size = os.path.isdir(file.path) and "-" or file.size()
                trs.append(
                    '<tr><td><a href="%s">%s</a></td><td>%s</td><td class="s">%s</td><td>%s</td></tr>' % (
                        file.cgi_name(), file.cgi_name(), file_lm, file_size,
                        self.description.get(file_name, "")))
        trs = "\n".join(trs)
        title = self.title
        print HTML % locals()
            

    def readme(self):
        SEP = "\nContents:\n"
        readme_dir = os.path.join(self.DIR_PATH, "README.txt")
        if os.path.exists(readme_dir):
            file = open(readme_dir)
            self.title = file.readline().strip() or self.title
            self.body = rest = file.read()
            desc = ""
            if SEP in rest:
                self.body, desc = rest.split(SEP, 1)
            self.body = markdown.markdown(self.body)
            self.description = {}
            for line in desc.splitlines():
                line = line.strip()
                if line:
                    key, value = line.split(":", 1)
                    self.description[key.strip()] = value.strip()


    def bzrbranch(self):
        cnt = 0
        for p in File(self.DIR_PATH).parents():
            dot_bzr = os.path.join(p, ".bzr")
            if os.path.exists(dot_bzr):
                # infer the bzr URL (guess)
                uri = os.environ['REQUEST_URI'].split('/')
                uri = '/'.join(uri[:-cnt-1]) # whoa! hard to explain, sorry.
                url = "http://%s%s" % (os.environ['SERVER_NAME'], uri)
                return p, url
            cnt += 1
        return False, False # not a bzr branch

HTML = r"""
<html>
  <head>
    <title>%(title)s</title>
    <style type="text/css">
    body { color: #333333; background-color: white; font-size: 14px; margin-bottom: 20px;
           font-family:"Lucida Grande","Bitstream Vera Sans","Verdana"; }
    div.box { padding-left: 5em; }
    a { color: #0033CC; }
    a:hover { background-color: #0033CC; color: white; text-decoration: none; }
    dl dt { font-weight: bold; }
    li { padding-bottom: 2px; }
    div#bzr { }
    div#bzr code { padding-left: 20px; }
    div#listdir { font-family: monospace;
                  background-color: #eeeeee;
                  border-bottom: 1px solid;
                  border-top: 1px solid;
                  margin-top: 12px;}
    div#listdir table { font-size: 13px; margin: 12px; }
    div#listdir th, td { text-align: left; padding-right: 14px; }
    div#listdir th { padding-bottom: 3px; }
    div#listdir td.s {text-align: right;}
    </style>
  </head>

  <body>
    <h1>%(title)s</h1>

    %(body)s

    <div id="listdir">
      <table>
        <thead>
        <tr>
          <th>Name</th>
          <th>Last Modified</th>
          <th>Size</th>
          <th>Description</th>
        </tr>
        </thead>
        <tbody>
          %(trs)s
        </tbody>
      </table>
    </div>
  </body>
</html>
"""
            

urls = (
    "", "lightydir",
    )
