#!/usr/bin/python -OO

import os
import web
import lightydir

# get over web.py's naive request processing
# .. that leads to unexpected behavior
os.environ['SCRIPT_NAME'] = os.environ['REQUEST_URI']

web.run(lightydir.urls, lightydir.__dict__, web.reloader)
